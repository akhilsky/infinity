import { Component,ElementRef,Inject, } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { LocalstorageService} from './services/localstorage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private elRef:ElementRef,private localstorageService : LocalstorageService ) { }
  public deviceData:any = null;
  isOn = false;
  deviceFormGroup: FormGroup;
  editMode = false;
  currentDeviceId = null;
  duplicate = false;

  ngOnInit(){
    this.deviceFormGroup = new FormGroup ({
      hostname : new FormControl('',[Validators.required,Validators.pattern('(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$')]),
      loopback : new FormControl('',[Validators.required,Validators.pattern('([0-9]{1,3}\.){3}[0-9]{1,3}$')])
  })
    this.getDevices();
  }

  get hostname() { return this.deviceFormGroup.get('hostname'); }
  get loopback() { return this.deviceFormGroup.get('loopback'); }
  
  private getDevices(){
      return  this.deviceData =this.localstorageService.getDevices();
  }
  interfacePopOver(deviceId){
    this.isOn = true;
    this.localstorageService.setDeviceId(deviceId)
  }
deleteEntry(deviceId){
  this.localstorageService.deleteDevice(deviceId)
  this.getDevices();
 }
 editDevice(deviceId){
  this.editMode = true;
  this.currentDeviceId = deviceId;
  var deviceData = this.localstorageService.getDeviceById(deviceId)
  this.deviceFormGroup.controls['hostname'].setValue(deviceData[0].hostname);
  this.deviceFormGroup.controls['loopback'].setValue(deviceData[0].loopback);
 }
registerDetails () {
  if(this.editMode){
    this.localstorageService.updateDevices(this.deviceFormGroup.value,this.currentDeviceId)
    this.editMode = false;
  }else{
    if(!this.localstorageService.saveDevices(this.deviceFormGroup.value))
      this.duplicate=true
    else  this.duplicate= false;
  }
  this.getDevices();
  
}
  
}


