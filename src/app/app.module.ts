import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient,HttpClientModule  } from '@angular/common/http'; 

import { AppComponent } from './app.component';
import { InterfacesComponent } from './interfaces/interfaces.component';
import { HeaderComponent } from './header/header.component';
import { InterfaceDirective } from './interfaces/interface.directive';
import { ShowErrorsComponent } from './helpers/show-errors/show-errors.component';

import { LocalstorageService} from './services/localstorage.service';



@NgModule({
  declarations: [
    AppComponent,
    InterfacesComponent,
    HeaderComponent,
    InterfaceDirective,
    ShowErrorsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
  ],
  providers: [LocalstorageService,HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
