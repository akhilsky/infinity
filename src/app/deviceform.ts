export class DeviceForm {
    id: number;
    hostname: string;
    loopback: string;
    constructor(values: Object = {}) {
      //Constructor initialization
      Object.assign(this, values);
  }
  
  }