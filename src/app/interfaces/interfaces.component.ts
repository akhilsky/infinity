import { Component, OnInit ,Input,ElementRef,SimpleChanges} from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { LocalstorageService} from '../services/localstorage.service';

@Component({
  selector: 'app-interfaces',
  templateUrl: './interfaces.component.html',
  styleUrls: ['./interfaces.component.css']
})
export class InterfacesComponent implements OnInit {

  constructor(private elRef:ElementRef,private localstorageService : LocalstorageService) { 
    
  }
  public interfacesData:any = null;
  interfaceFormGroup:FormGroup;
  editMode = false;
  currentinterfaceId;
  duplicate = false;

  ngOnInit(){
    this.interfaceFormGroup = new FormGroup ({
      hostname : new FormControl('',[Validators.required,Validators.pattern('(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$')]),
      ip : new FormControl('',[Validators.required,Validators.pattern('([0-9]{1,3}\.){3}[0-9]{1,3}$')])
  })
  this.interfacesData = this.getInterfaces();
 }
 ngDoCheck(){
  this.interfacesData = this.getInterfaces();
 }
 
 registerInterfaces () {
  if(this.editMode){
    this.localstorageService.updateInterfaces(this.interfaceFormGroup.value,this.currentinterfaceId)
    this.editMode = false;
  }else{
   
  if(!this.localstorageService.saveInterfaces(this.interfaceFormGroup.value))
    this.duplicate=true
    else  this.duplicate= false;
  }
  this.interfacesData = this.getInterfaces();
  
}
editInterfaces(interfaceId){
  this.editMode = true;
  this.currentinterfaceId = interfaceId;
  var interfaceData = this.localstorageService.getInterfaceById(interfaceId)
  this.interfaceFormGroup.controls['hostname'].setValue(interfaceData[0].hostname);
  this.interfaceFormGroup.controls['ip'].setValue(interfaceData[0].ip);
 }
private getInterfaces(){
  return  this.localstorageService.getInterfacesData();
}
deleteInterface(interfaceId){
  this.localstorageService.deleteInterfaces(interfaceId)
  this.interfacesData = this.getInterfaces();
 }
}
