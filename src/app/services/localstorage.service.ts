import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class LocalstorageService {
  deviceData = [];
  interFacesData = [];
  savedInterFacesData = null;
  savedDevices = [];
  deviceId:Number = null;

  constructor(private http: HttpClient ) { }

  public saveDevices(deviceData): any {
    this.savedDevices =  this.getDevices();
      if(!this.savedDevices)
      this.savedDevices = [];
    if (this.ifExists(this.savedDevices,deviceData))
      return false;   
    else{
      this.savedDevices.push(deviceData);
      localStorage.setItem('devices1', JSON.stringify(this.savedDevices));
      return true;
    }
    
  }
  private ifExists(existingData,deviceData){
    var counter = 0;
    existingData.map(function(value){
     if(value.hostname == deviceData.hostname || value.loopback == deviceData.loopback){
      counter = counter +1
     }
    })
    if(counter> 0)
    return true;
    else return false;
  }
  public updateDevices(deviceData,index) {
    this.savedDevices =  this.getDevices(); 
    this.savedDevices[index] = deviceData;
    localStorage.setItem('devices1', JSON.stringify(this.savedDevices));
    
  }
  public updateInterfaces(interfaceData,index) {
    this.savedInterFacesData =  this.getInterfacesData(); 
    this.savedInterFacesData[index] = interfaceData;
    localStorage.setItem('interface', JSON.stringify(this.savedInterFacesData));
    
  }
  public getDevices(): Array<any[]>  {
    this.savedDevices =  JSON.parse(localStorage.getItem('devices1'));  
    return this.savedDevices;
  }
  public setDeviceId(id){
    this.deviceId = id;
  }
  public deleteDevice(id){
    this.savedDevices =  JSON.parse(localStorage.getItem('devices1'));  
    this.savedDevices=this.savedDevices.filter((x, i) => i !== id);
    localStorage.setItem('devices1', JSON.stringify(this.savedDevices));
  }
  public getInterfacesData():any{
    this.savedInterFacesData =  JSON.parse(localStorage.getItem('interface'));
    if(this.savedInterFacesData)
    return this.savedInterFacesData.filter((x, i) => x.deviceId === this.deviceId);
     
  }
  public deleteInterfaces(id){
    this.savedInterFacesData =  JSON.parse(localStorage.getItem('interface'));
    this.savedInterFacesData=this.savedInterFacesData.filter((x, i) => i !== id);;
    localStorage.setItem('interface', JSON.stringify(this.savedInterFacesData));
  }
  public  getDeviceById(deviceId) {
    this.savedDevices =  JSON.parse(localStorage.getItem('devices1'));
    return this.savedDevices.filter((x, i) => i === deviceId);
  }
  public  getInterfaceById(interfaceId) {
    this.savedInterFacesData =  JSON.parse(localStorage.getItem('interface'));
    return this.savedInterFacesData.filter((x, i) => i === interfaceId);
  }
  public saveInterfaces(interFaceData): any {
    var interfacesObj = {
      'deviceId':this.deviceId,
      'hostname':interFaceData.hostname,
      'ip':interFaceData.ip
    }
    this.savedInterFacesData =  JSON.parse(localStorage.getItem('interface'));
    if(!this.savedInterFacesData)
    this.savedInterFacesData = [];
    if (this.ifExistsInterface(this.savedInterFacesData,interFaceData))
      return false;   
      else{
      this.savedInterFacesData.push(interfacesObj)
      localStorage.setItem('interface', JSON.stringify(this.savedInterFacesData));
      return true;
    }
  }
  private ifExistsInterface(existingData,interfaceeData){
    var counter = 0;
    existingData.map(function(value){
     if(value.hostname == interfaceeData.hostname || value.ip == interfaceeData.ip){
      counter = counter +1
     }
    })
    if(counter> 0)
    return true;
    else return false;
  }

}
